## Scenario

- After booting up, SPi waits for the command from the RPi.

- RPi will launch `rpi_photo.py` at the booting time and will execute 'set_next_boot_time' function.

    This function reads the schedule from the `config.xml` file and calculate next booting time in  minutes.
    
    And then it sends it to the SPi.
    
- After receiving next booting time, SPi sets this value to the RTC and goes into sleep mode.

- RTC will wake SPi up when next booting time.


## Working with Raspberry Pi

### Preparing Raspberry Pi.
    
- Download RPi image file from below and install it.
    
    https://www.dropbox.com/s/7fnmwziybszvci9/Jessie-SleepyPi-Nov15.zip?dl=0
    
- Enable picamera with `sudo raspi-config`
  

### Install dependencies

    sudo apt-get update
    sudo apt-get install python-picamera
    sudo pip install pyserial boto
    
### Enable auto-starting
    
    sudo nano /etc/profile
    
And add follow line at the end of file.
    
    sudo python /home/pi/rpi_photo/rpi_photo.py
    
    
## Working with Cloud Server
    
### Install dependencies
    
    sudo apt-get install python-dev python-pip
    sudo apt-get install libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk
    sudo pip install pillow
    sudo pip install boto

### Enable auto starting

- Image downloader

        sudo nano /etc/rc.local
    
    And add below lines before `exit 0`
    
        /usr/bin/python /home/ubuntu/greenpole_server/sync_img.py &
    
- Web server
    
        sudo nano /home/ubuntu/web_server.sh
        
    And add below lines
        
        #!/bin/bash
        cd /home/ubuntu/greenpole_server
        /usr/bin/python imageme.py
    
    And make it executable.
        
        sudo chmod +x /home/ubuntu/web_server.sh
    
    Open the `rc.local`
    
        sudo nano /etc/rc.local
        
    Add below line
    
        /bin/bash /home/ubuntu/web_server.sh &
    
Reboot system and you can see simple web page in **http://127.0.0.1/greenpole.html**

## Configuring **Huawei 312U Aircard** 4g dongle for internet.

### Testing modem.
    
    sudo apt-get update
    sudo apt-get install ppp wvdial
    sudo nano /etc/wvdial.conf
    
And modify original file to below:
    
    [Dialer Defaults]
    Modem = /dev/ttyUSB3
    Baud = 460800
    Dial = ATDT
    Init1 = ATZ
    Init2 = AT+CGDCONT=1,"IP","yesinternet"
    Init3 = AT$QCPDPP=1,0
    phone = *99#
    Ask Password = 0
    Stupid Mode = 1
    Username = notused
    Password = notused
    Dial Attempts = 3

Press **CTRL+X, Y** to save & exit.

To start internet, just type `sudo wvdial`

Open another terminal and type below to check internet.
    
    ping -I ppp0 www.google.com
    
# Enabling auto-start.
    
    sudo nano /etc/rc.local
    
And add following line before `exit 0`
    
    (sleep 10; /usr/bin/wvdial)&
    

