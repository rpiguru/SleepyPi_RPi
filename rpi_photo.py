import csv
import glob
import socket
import urllib2

import boto
import datetime
import os
import logging
import serial
import xml.etree.ElementTree
import time
from boto.s3.key import Key

conf_file_name = '/home/pi/rpi_photo/config.xml'
# conf_file_name = 'config.xml'

debug = False


def get_param_from_xml(param):
    """
    Get configuration parameters from the config.xml
    :param param: parameter name
    :return: if not exists, return None
    """
    root = xml.etree.ElementTree.parse(conf_file_name).getroot()
    tmp = None
    for child_of_root in root:
        if child_of_root.tag == param:
            tmp = child_of_root.text
            break

    return tmp


def set_param_to_xml(tag_name, new_val):
    et = xml.etree.ElementTree.parse(conf_file_name)
    for child_of_root in et.getroot():
        if child_of_root.tag == tag_name:
            child_of_root.text = new_val
            et.write(conf_file_name)
            return True
    return False

aws_bucket = get_param_from_xml('AWS_BUCKET_NAME')
aws_key = get_param_from_xml('AWS_KEY')
aws_security = get_param_from_xml('AWS_SECRET')


class RPiPhoto:

    camera = None
    ser = None

    def __init__(self):
        try:
            self.ser = serial.Serial('/dev/ttyS0', baudrate=9600, timeout=5)

            import picamera
            self.camera = picamera.PiCamera()
            self.camera.resolution = (2592, 1944)
            self.check_img_dir()
        except:
            logging.error('Failed to import camera module..')

    @staticmethod
    def check_img_dir():
        img_dir = '/home/pi/rpi_photo/img'
        if not os.path.exists(img_dir):
            os.makedirs(img_dir)

    def take_pic(self):
        """
        Take photo and save it, and return its full path
        :return:
        """
        try:
            f_path = "/home/pi/rpi_photo/img/" + datetime.datetime.now().strftime("%Y_%m_%d__%H_%M_%S") + ".jpg"
            self.camera.capture(f_path)
            return f_path
        except:
            print "Failed to take photo"
            logging.error("Failed to take photo")
            return None

    def upload_to_cloud(self, file_path):
        f_name = os.path.basename(file_path)
        if self.push_picture_to_s3(f_name, file_path):
            try:
                os.remove(file_path)
                return True
            except OSError as er:
                print er
                logging.error(er)
                return False
        else:
            return False

    def set_next_boot_time(self):
        """
        Set next boot time to the SPi via serial.
        - Calculate next boot time in minutes
        - Send it in type of HH:MM
        :return:
        """
        if not self.sync_with_spi():
            print "Failed to sync with SPi"
            logging.error("Failed to sync with SPi")
            return False

        # Calculate minutes to the next booting time
        time_on = get_param_from_xml('TIME_ON').split(':')
        time_off = get_param_from_xml('TIME_OFF').split(':')
        time_on = [int(t) for t in time_on]
        time_off = [int(t) for t in time_off]

        minute_on = time_on[0] * 60 + time_on[1]
        minute_off = time_off[0] * 60 + time_off[1]
        t_now = datetime.datetime.now()
        minute_now = t_now.hour * 60 + t_now.minute

        next_time = [0, 10]
        if minute_on <= minute_now <= minute_off:
            next_time = get_param_from_xml('INTERVAL').split(':')
            next_time = [int(t) for t in next_time]
        else:
            # calculate minutes from 17:00 ~ 6:00 of next day
            off_hour = time_on[0] + 24 - time_off[0]
            off_min = time_on[1] - time_off[1]
            if off_min < 0:
                off_min += 60
                off_hour -= 1

            next_time = [off_hour, off_min]

        # Update new value to the SPi
        print "Next remaining time: ", next_time

        self.ser.write(str(unichr(next_time[0])))
        try:
            recv = int(self.read_serial_data())
            if recv != next_time[0]:
                print "Invalid response for the first data: ", recv
                return False
            self.ser.write(str(unichr(next_time[1])))
            recv = int(self.read_serial_data())
            if recv != next_time[1]:
                print "Invalid response for the 2nd data: ", recv
                return False
        except ValueError:
            print "Failed to receive ACK from the SPi"
            return False

        recv = self.read_serial_data()
        print "Next booting time: ", recv, ' min'

        print "Succeeded to update next boot time."
        logging.info("Succeeded to update next boot time -> %s:%s" % tuple(next_time))

        return True

    def update_voltage(self):
        """
        Read battery output voltage and save to csv file, upload to cloud
        :return:
        """
        vol = self.read_serial_data()
        msg = 'Battery voltage: %s V' % vol
        logging.info(msg)
        print msg

        time_str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        f_exist = False
        adc_log_file_name = get_param_from_xml('CSV_FILE')
        if os.path.exists(adc_log_file_name):
            f_exist = True

        m_file = open(adc_log_file_name, 'a')
        wr = csv.writer(m_file, quoting=csv.QUOTE_MINIMAL)

        if not f_exist:
            wr.writerow(['Date & Time', 'Voltage'])
        wr.writerow([time_str, vol])

        m_file.close()

        self.upload_to_cloud(adc_log_file_name)

    def sync_with_spi(self):
        """
        Synchronize with SPi by sending/receiving delimiters
        :return:
        """
        print "Dummy data: ", self.read_serial_data()  # popup dummy data

        # SYNC word is [50, 100, 50, 75]
        l_sync = [50, 100, 50, 75]
        try:
            for syn in l_sync:
                self.ser.write(str(unichr(syn)))
                recv = int(self.read_serial_data())
                print recv
                if recv != syn:
                    return False
            return True
        except ValueError as e:
            print e
            return False

    def read_serial_data(self):
        """
        Read data line from the serial
        :return:
        """
        line = ''
        s_time = time.time()
        while True:
            if time.time() - s_time >= 1:
                break
            try:
                data = self.ser.read()
                if data == '\r':
                    break
                else:
                    line += data
            except serial.SerialException as er:
                print er

        return line.strip()

    def push_picture_to_s3(self, file_key, file_path):
        try:
            # connect to the bucket
            conn = boto.connect_s3(aws_key, aws_security)
            bucket = conn.get_bucket(aws_bucket)

            msg = "Uploading to AWS.. Key: " + file_key + "  Path: " + file_path
            print msg
            logging.info(msg)

            k = Key(bucket)
            k.key = file_key
            k.set_contents_from_filename(file_path)
            # we need to make it public so it can be accessed publicly
            # using a URL like http://s3.amazonaws.com/bucket_name/key
            k.make_public()
            print "Succeeded to upload to AWS S3..."
            logging.info("Succeeded to upload to AWS S3...")
            return True
        except ValueError as er:
            print er
            logging.error(er)
            return False
        except socket.gaierror as er:
            print er
            logging.error(er)
            return False
        except boto.exception.S3ResponseError as er:
            print er
            logging.error(er)
            return False

    def upload_remaining_imgs__tmp(self):
        """
        Upload remaining images to cloud
        :return:
        """
        f = open('/home/pi/rpi_photo/not_uploaded.txt', 'a+b')
        remaining_images = []
        for f_name in f.readlines():
            if len(f_name.strip()) > 0:
                if f_name.endswith('\n'):
                    f_name = f_name[:-1]
                if not self.upload_to_cloud(f_name):  # failed again? save again...., damn...
                    remaining_images.append(f_name)
                else:
                    os.remove(f_name)

        f.seek(0)
        f.truncate()
        f.write('\n'.join(remaining_images))
        f.close()

    def upload_remaining_imgs(self):
        """
        If file count is greater than 100, remove some files...
        :return:
        """
        local_list = glob.glob('/home/pi/rpi_photo/img/*.jpg')
        for f_path in local_list:
            if self.upload_to_cloud(f_path):
                return True
            else:
                logging.error('Failed to upload old file: ', f_path)
                return False


def sync_time():
    os.system('/etc/init.d/ntp stop')
    os.system('ntpd -qg')
    time.sleep(5)
    os.system('/etc/init.d/ntp start')


def check_internet():
    """
    Check internet connection
    :return:
    """
    try:
        response = urllib2.urlopen('https://www.google.com.au', timeout=1)
        val = True
    except urllib2.URLError as e:
        val = False

    print 'Checking internet connection... ', val
    return val
    # return False


def store_img_file(img_file_path):
    """
    Store file name to the txt file when there is no internet connection.
    Once repaired internet, they will be uploaded...
    :return:
    """
    f = open('/home/pi/rpi_photo/not_uploaded.txt', "a+b")
    f_content = f.readlines()
    f_content.append(img_file_path)
    f.seek(0)
    f.truncate()
    f.write('\n'.join(f_content))
    f.close()


if __name__ == '__main__':
    # time.sleep(30)
    # sync_time()

    log_file_name = '/home/pi/rpi_photo/log.txt'
    logging.basicConfig(level=10, filename=log_file_name, format='%(asctime)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.getLogger('boto').setLevel(logging.ERROR)

    logging.info("")
    try:
        ctrl = RPiPhoto()

        img_path = ctrl.take_pic()

        if img_path is not None:
            if not check_internet():
                # store_img_file(img_path)
                logging.error('Failed to connect to internet, saved file: ' + img_path)
            else:
                if ctrl.upload_to_cloud(img_path):
                    ctrl.upload_remaining_imgs()

        b_t = ctrl.set_next_boot_time()

        if not b_t:
            logging.error("Failed to set next boot time")

        ctrl.update_voltage()

        ctrl.upload_to_cloud(log_file_name)

        if not debug and b_t:
            os.system('sudo shutdown -h now')

    except ValueError as e:
        print e
        logging.error(e)





