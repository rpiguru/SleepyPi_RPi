import glob
import os
import boto
import time
from boto.s3.key import Key
from boto.s3.connection import S3Connection

BUCKET_NAME = 'greenpole01'
AWS_KEY = 'AKIAIAMWB36ESP55NDSQ'
AWS_SECRET = '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'


def get_file_list():
    # connect to the bucket
    conn = boto.connect_s3(AWS_KEY, AWS_SECRET)
    bucket = conn.get_bucket(BUCKET_NAME)
    # go through the list of files
    bucket_list = bucket.list()
    re = []
    for l in bucket_list:
        f_name = str(l.key)
        re.append(f_name)

    return re


def download_file():
    # connect to the bucket
    conn = boto.connect_s3(AWS_KEY, AWS_SECRET)
    bucket = conn.get_bucket(BUCKET_NAME)
    # go through the list of files
    bucket_list = bucket.list()
    for l in bucket_list:
        f_name = str(l.key)
        # check if file exists locally, if not: download it
        f_path = '/home/ubuntu/greenpole_server/' + f_name
        if not os.path.exists(f_path):
            l.get_contents_to_filename(f_path)


def clean_local():
    """
    If file count is greater than 100, remove some files...
    :return:
    """
    local_list = glob.glob('/home/ubuntu/greenpole_server/*.jpg')
    local_list.sort()
    if len(local_list) > 100:
        for i in range(len(local_list) - 100):
            os.remove(local_list[i])

if __name__ == '__main__':

    while True:
        s_time = time.time()

        print "-- Remote file list ---"
        f_list = get_file_list()
        for f in f_list:
            print f

        download_file()

        clean_local()

        while True:
            if time.time() - s_time > 180:
                break
            else:
                time.sleep(5)

